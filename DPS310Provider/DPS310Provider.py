# PyWeather 3 - DPS310 Pressure Sensor Provider Module
# Based on Adafruit DPS310 library
# (c) 2021 Owen McGinley
# Licensed under MIT License

import adafruit_dps310

class DPS310Provider:
    def __init__(self, i2c):
        self.i2c = i2c
        self.dps310 = adafruit_dps310.DPS310(i2c)

    def readtemp(self, units):
        if units == "F":
            return (self.dps310.temperature * (9/5)) + 32
        else:
            return self.dps310.temperature

    def readpressure(self):
        return self.dps310.pressure