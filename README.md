# PyWeather 3
Code for running the weather display I have.

# Spaghet-o-meter
Mostly not spaghetti with some parts of code I could improve to make more modular. Each data section has it's own module, separate providers for ClimaCell/Dark Sky. 2/10.

# PyWeather 3 demo images
PyWeather 3 demo images can be found on my website. Link is below.

https://owenthe.dev/pyweather-3

# Changes coming to PyWeather 3
I should be adding some extra features to PyWeather 3 in the coming weeks, which include:
* Rewriting the alerts system
* Pinging a slack threshold when certain indoor data crosses a threshold
* Trying to not use DynamoDB for data storage (probably making a middleman API on my web server)

# Why make PyWeather 3?
PyWeather 3 was meant to be a refined revision of PyWeather 2. PyWeather 3 was made for a few reasons:
* Experimenting with larger e-paper displays and being able to show more detail.
* Keeping what worked well in PyWeather 2, modifying what didn't, and using the extra display space to have more weather information.
* To keep even closer tabs on indoor environmental data - now including light levels, air quality, and pressure.
* The display used has 4 GPIO buttons, meaning I could make different displays for PyWeather 3.

After ordering parts, I mostly made PyWeather 3 over winter break.

Aside from the last point, everything about PyWeather 3 went great! I made some refinements as the year went on, but it was a really nice staple at my desk.

When I injured my tailbone and couldn't sit at my desk for 2 weeks, I brought PyWeather 2 to my bedside, so I could keep an eye on the weather.

# Hardware Setup
PyWeather 3 is relatively modular, but has been fully tested and works best on this hardware setup:
* Pimoroni Inky Impression display (600x448 e-ink display @ 5.7 inches. Theoretically this will work on any 600x448 e-ink display, but will need modification to the code.)
* SHTC3 temperature/humidity sensor
* PMSA300I air quality sensor
* BH1750 light sensor
* DPS310 pressure sensor
* Raspberry Pi 4 running Raspbian (although you can easily get away with a Pi 3/3B+)

Total parts cost was about ~$130 USD.

At the end of the day, you can get away with a BME680, BH1750, PMSA300I, and the display which is slightly cheaper. You can also do away with some indoor data, although you'll need to modify some code to disable that functionality.

I chose these individual sensors for a good balance between accuracy and cost. Everything was bought from Adafruit, and all these sensors have a Stemma QT variant for no-soldering plug and play.

# Software Setup
To get started with PyWeather 3 in the default software configuration, you'll need:
* Python 3.5 or higher (my Pi 4 has 3.7.3 installed).
* The requests, boto3, and Pillow libraries
* Standard I2C libraries (`board`, `busio`).
* Specific adafruit circuitpython libraries for sensors being used (setup instructions are available on the Adafruit help pages for each sensor)
* Dark Sky AND ClimaCell API keys. Dark Sky is still primarily used for grabbing the weather data, but I also included a ClimaCell Provider for minutely data. With some code modification and by passing a ClimaCell Provider, you can get this to work 100% with ClimaCell.

# Configuration Setup
There's some configuration sections for PyWeather 3. All are pretty self explanatory.

## AWS section
Everything here is pretty self-explanatory. PyWeather 3 uses DynamoDB for storage, so you'll need to enter AWS Access/Secret Keys that can write to the DynamoDB storing that data.

The `enabled` key will turn on/off the entire thing.

1 read/write capacity unit should work just fine, increase the read capacity if you have a lot of traffic on 

`dbname` is where the name of the database you're storing data in. `region` is the region code of where the DynamoDB database is in (e.g. `us-east-2`). `purgetime` is how long data can remain in the database - the default is `259200` (72 hours).

Unlike PyWeather 2, PyWeather 3 can do retroactive deletion! Just note - with 1 write capacity unit, retroactively deleting a lot of data can take up to 7-9 minutes.

## DARKSKY section
One key here - `key`. Put in your Dark Sky API key here.

## CLIMACELL section
One key here - `key`. Put in your Climacell API key here.

## LOCATION section
Put the `latitude` and `longitude` of where you want to check the weather for here.

## INKY section
If you'd like to instead preview the output of PyWeather 3, you can set `disabled` to True, and not output to the Inky Impression.

`flipped` determines if the output image is flipped around 180 degrees. This is False by default.

Set this to True if you install PyWeather 3 such that the white "bezel" is facing upwards (e.g. the Pi power connector/I2C wires come out of the relative *top* of the display.)

# Cron configuration
PyWeather 3 does not have a caching system, so a simple cronjob to run PyWeather 3 every x minutes works.

Every 5 minutes is a good refresh rate (288 refreshes/day). This will keep you way under Dark Sky/Climacell free usage limits. 

Set this up in cron via these lines (for a 5-minute refresh rate):

`@reboot /home/pi/startpw3.sh >> /dev/null 2>&1`
`0,5,10,15,20,25,30,35,40,45,50,55 * * * * /home/pi/startpw3.sh >> /dev/null 2>&1`

`startpw3.sh` is a simple shell script that launches PyWeather 3 in it's relative directory. It's included in the files. The location of your `startpw3.sh` may be different.

You can exclude the `>> /dev/null 2>&1` if you don't have a local mailing system, or want to get mail if the cronjobs fail.

Once that's all up and running - PyWeather 3 should be running. Enjoy!

# License
PyWeather 3 is licensed under the MIT license.

PyWeather 3 includes Weather Icons and the Roboto font.
