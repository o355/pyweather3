# PyWeather 3 - Dark Sky Provider Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

import requests
from datetime import datetime
from dateutil import tz
import json


class DarkSkyProvider:
    def __init__(self, apikey, latitude, longitude):
        self.data = {}
        self.apikey = apikey
        self.latitude = latitude
        self.longitude = longitude
        self.tz = None

    def loaddata(self, filename):
        with open(filename) as f:
            self.data = json.load(f)

    def dumpdata(self, filename):
        with open(filename, "w") as f:
            json.dump(self.data, f)

    def gettz(self):
        return self.data['timezone']

    def gettzobj(self):
        return self.tz

    def requestdata(self):
        data = requests.get("https://api.darksky.net/forecast/%s/%s,%s" % (self.apikey, self.latitude, self.longitude))
        self.data = data.json()
        self.tz = tz.gettz(self.data['timezone'])

    def getcurrently(self, field):
        return self.data['currently'][field]

    def gethourly(self, hour, field):
        if field == "summary":
            return self.data['hourly'][field]

        return self.data['hourly']['data'][hour][field]

    def getdaily(self, day, field):
        if field == "summary":
            return self.data['daily'][field]

        return self.data['daily']['data'][day][field]

    def getminutely(self, minute, field):
        if field == "summary" or field == "icon":
            return self.data['minutely'][field]

        return self.data['minutely']['data'][minute][field]

    def getalerts(self, index, field):
        return self.data['alerts'][index][field]

    def getalertslength(self):
        return len(self.data['alerts'])

    def translateicon(self, icon, summary=None, rate=None):
        translations = {"clear-day": u"\uf00d", "clear-night": u"\uf02e", "rain": u"\uf019", "snow": u"\uf01b",
                        "sleet": u"\uf0b5", "wind": u"\uf050", "fog": u"\uf014", "cloudy": u"\uf013",
                        "partly-cloudy-day": u"\uf002", "partly-cloudy-night": u"\uf086", "hail": u"\uf015",
                        "thunderstorm": u"\uf01e", "tornado": u"\uf056"}
        if summary is None and rate is None:
            return translations[icon]
        else:
            # Precip Rate checking first
            if rate is not None:
                if icon == "rain":
                    if rate < 0.01:
                        return u"\uf01c"
                    elif rate < 0.05:
                        return u"\uf01a"
                    else:
                        return translations[icon]
                else:
                    return translations[icon]

            # Then summary check
            if summary.find("Drizzle") != -1:
                return u"\uf01c"
            elif summary.find("Light Rain") != -1:
                return u"\uf01a"
            else:
                return translations[icon]

    def getsunrisetime(self):
        try:
            return self.getdaily(0, "sunriseTime")
        except KeyError:
            return 0

    def getsunsettime(self):
        try:
            return self.getdaily(0, "sunsetTime")
        except KeyError:
            return 0

    def translatebearing(self, bearing):
        bearing = abs(bearing - 180)
        if bearing < 22.5:
            return u"\uf058"
        elif bearing < 67.5:
            return u"\uf057"
        elif bearing < 112.5:
            return u"\uf04d"
        elif bearing < 157.5:
            return u"\uf088"
        elif bearing < 202.5:
            return u"\uf044"
        elif bearing < 247.5:
            return u"\uf043"
        elif bearing < 292.5:
            return u"\uf048"
        elif bearing < 337.5:
            return u"\uf087"
        else:
            return u"\uf058"

