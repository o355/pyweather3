# PyWeather 3 - BH1750 Light Sensor Provider Module
# Based on Adafruit BH1750 library
# (c) 2021 Owen McGinley
# Licensed under MIT License

import adafruit_bh1750

class BH1750Provider:
    def __init__(self, i2c):
        self.i2c = i2c
        self.bh1750 = adafruit_bh1750.BH1750(self.i2c)

    def read(self):
        return self.bh1750.lux