# PyWeather 3 - AWS Upload Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

import boto3
import time


class AWSUpload:
    def __init__(self, access_key, secret_key, region, dbname, purgetime):
        self.access_key = access_key
        self.secret_key = secret_key
        self.region = region
        self.dbname = dbname
        self.purgetime = purgetime

        self.session = boto3.Session(aws_access_key_id=self.access_key, aws_secret_access_key=self.secret_key, region_name=self.region)
        self.db = self.session.resource('dynamodb')
        self.table = self.db.Table(self.dbname)

    def upload(self, intemp, intempd, inhum, inhumd, inpress, inpm10, inpm25, inpm100, inaqi, indp):
        self.table.put_item(
            Item={
                'timestamp': int(time.time() // 60 * 60),
                'insidetemp': repr(intemp),
                'insidetempdelta': repr(intempd),
                'insidehum': repr(inhum),
                'insidehumd': repr(inhumd),
                'inpress': repr(inpress),
                'inpm10': repr(inpm10),
                'inpm25': repr(inpm25),
                'inpm100': repr(inpm100),
                'inaqi': repr(inaqi),
                'indp': repr(indp)
            }
        )

    def delete(self):
        response = self.table.scan()
        purgestamp = int((time.time() // 60 * 60) - self.purgetime)
        for i in response['Items']:
            timestamp = int(i['timestamp'])
            if timestamp < purgestamp:
                try:
                    self.table.delete_item(
                        Key={
                            'timestamp': timestamp
                        }
                    )
                except:
                    pass
