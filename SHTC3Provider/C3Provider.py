# PyWeather 3 - SHTC3 Temperature/Humidity Sensor Provider
# (c) 2021 Owen McGinley
# Licensed under MIT License

import adafruit_shtc3
import math


class C3Provider:
    def __init__(self, i2c):
        self.i2c = i2c
        self.shtc3 = adafruit_shtc3.SHTC3(i2c)

    def readtemp(self, units):
        if units == "F":
            return (self.shtc3.temperature * (9/5)) + 32
        else:
            return self.shtc3.temperature

    def readhum(self):
        return self.shtc3.relative_humidity

    def get_dew_point(self, t_air_f, rel_humidity):
        t_air_c = (t_air_f - 32) * 5 / 9
        A = 17.27
        B = 237.7
        alpha = ((A * t_air_c) / (B + t_air_c)) + math.log(rel_humidity / 100.0)
        dew_point_c = (B * alpha) / (A - alpha)
        return (dew_point_c * 9 / 5) + 32