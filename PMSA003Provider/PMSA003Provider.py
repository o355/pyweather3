# PyWeather 3 - PMSA003 Air Quality Sensor Provider
# Based on Adafruit PM25 Library
# (c) 2021 Owen McGinley
# Licensed under MIT License

from adafruit_pm25.i2c import PM25_I2C


class PMSA003Provider:
    def __init__(self, slow_i2c, reset_pin):
        self.slow_i2c = slow_i2c
        self.reset_pin = reset_pin

        self.pm25 = PM25_I2C(self.slow_i2c, self.reset_pin)

    def read(self):
        return self.pm25.read()

    def returnaqifill(self, aqi):
        if aqi <= 50:
            return 0, 228, 0
        elif aqi <= 100:
            return 255, 255, 0
        elif aqi <= 150:
            return 255, 126, 0
        elif aqi <= 200:
            return 255, 0, 0
        elif aqi <= 300:
            return 153, 0, 76
        else:
            return 126, 0, 35

    def usaqi(self, concentration):
        ihigh = 0
        ilow = 0
        chigh = 0
        clow = 0

        if concentration <= 12:
            ihigh = 50
            ilow = 0
            chigh = 12
            clow = 0
        elif concentration <= 35.4:
            ihigh = 100
            ilow = 51
            chigh = 35.4
            clow = 12.1
        elif concentration <= 55.4:
            ihigh = 150
            ilow = 101
            chigh = 55.4
            clow = 35.5
        elif concentration <= 150.4:
            ihigh = 200
            ilow = 151
            chigh = 150.4
            clow = 55.5
        elif concentration <= 250.4:
            ihigh = 300
            ilow = 201
            chigh = 250.4
            clow = 150.5
        elif concentration <= 350.4:
            ihigh = 400
            ilow = 301
            chigh = 350.4
            clow = 250.5
        elif concentration <= 500.4:
            ihigh = 500
            ilow = 401
            chigh = 500.4
            clow = 350.5
        else:
            return 501

        return (((ihigh - ilow) / (chigh - clow)) * (concentration - clow)) + ilow