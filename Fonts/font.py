# PyWeather 3 - Font Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

from PIL import ImageFont


class Font:
    def __init__(self, fontfile, lgsize, normalsize, smsize):
        self.small_font = ImageFont.truetype(fontfile, smsize)
        self.font = ImageFont.truetype(fontfile, normalsize)
        self.large_font = ImageFont.truetype(fontfile, lgsize)