# PyWeather 3 - "Pod" for main display
# (c) 2021 Owen McGinley
# Licensed under MIT License

from PIL import Image, ImageDraw


class Pod:
    def __init__(self, width, height, textfont, iconfont, datetime, strffmt, specialdenom, icon, temperature, precipChance, windSpeed, windDir, precipcutoff, dsp, temperaturelow):
        self.width = width
        self.height = height
        self.textfont = textfont
        self.iconfont = iconfont
        self.datetime = datetime
        self.icon = icon
        self.temperature = temperature
        self.precipChance = precipChance
        self.windSpeed = windSpeed
        self.windDir = windDir
        self.precipcutoff = precipcutoff
        self.dsp = dsp
        self.temperaturelow = temperaturelow
        self.strffmt = strffmt
        self.specialdenom = specialdenom
        self.image = Image.new("RGB", (self.width, self.height), (255, 255, 255))
        self.draw = ImageDraw.Draw(self.image)

    def render(self):
        timefmt = self.datetime.strftime(self.strffmt)
        extraiconheight = 0
        if self.specialdenom:
            test_fmt_a = timefmt + "a"
            test_fmt_p = timefmt + "p"

            width_a, height_a = self.draw.textsize(test_fmt_a, font=self.textfont.font)
            width_p, height_p = self.draw.textsize(test_fmt_p, font=self.textfont.font)

            denomfmt = self.datetime.strftime("%p")
            if denomfmt == "AM":
                denom = "a"
                if height_a > height_p:
                    extraiconheight = height_p - height_a
            elif denomfmt == "PM":
                denom = "p"
                if height_p > height_a:
                    extraiconheight = height_a - height_p
            timefmt = timefmt + denom
        timew, timeh = self.draw.textsize(timefmt, font=self.textfont.font)
        mask = self.iconfont.font.getmask(self.icon)
        # Hard coded value. Should be the size of the text font in px.
        fonth, fontw = 34, 34
        iconw, iconh = mask.size
        iconrw, iconrh = self.draw.textsize(self.icon, font=self.iconfont.font)
        if self.temperaturelow != None:
            temptext = self.temperature + " | " + self.temperaturelow
        else:
            temptext = self.temperature + "°F"
        tempw, temph = self.draw.textsize(temptext, font=self.textfont.font)
        self.draw.text(((self.width - timew) / 2, 5), timefmt, font=self.textfont.font, fill=(0, 0, 0))
        self.draw.text((((self.width - iconw) / 2), 5 + timeh + extraiconheight + 2 + ((fonth - iconh) / 2) - (iconrh - iconh)), self.icon, font=self.iconfont.font, fill=(0, 0, 0))
        self.draw.text((((self.width - tempw) / 2), 60), temptext, font=self.textfont.font, fill=(0, 0, 0))
        if self.precipChance < self.precipcutoff:
            umw, umh = self.draw.textsize(self.dsp.translatebearing(self.windDir), font=self.iconfont.large_font)
        else:
            umw, umh = self.draw.textsize(u"\uf084", font=self.iconfont.small_font)

        if self.precipChance == -1:
            utextw, utexth = self.draw.textsize("N/A%", font=self.textfont.font)
            totalw = umw + 5 + utextw
        elif self.precipChance < self.precipcutoff:
            utextw, utexth = self.draw.textsize(str(int(round(self.windSpeed, 0))), font=self.textfont.font)
            totalw = umw + 6 + utextw
        else:
            utextw, utexth = self.draw.textsize(str(self.precipChance) + "%", font=self.textfont.font)
            totalw = umw + 5 + utextw

        if self.precipChance == -1:
            self.draw.text((((self.width - totalw) / 2) - 5, 79), u"\uf084", font=self.iconfont.small_font, fill=(0, 0, 0))
            self.draw.text((((self.width - totalw) / 2) + umw + 8, 81), "N/A%",
                           font=self.textfont.font, fill=(0, 0, 0))
        elif self.precipChance < self.precipcutoff:
            self.draw.text((((self.width - totalw) / 2), 70), self.dsp.translatebearing(self.windDir),
                           font=self.iconfont.large_font, fill=(0, 0, 0))
            self.draw.text((((self.width - totalw) / 2) + umw + 6, 81), str(int(round(self.windSpeed, 0))),
                           font=self.textfont.font, fill=(0, 0, 0))
        else:
            self.draw.text(((self.width - totalw) / 2, 79), u"\uf084", font=self.iconfont.small_font, fill=(0, 0, 0))
            self.draw.text((((self.width - totalw) / 2) + umw + 5, 81), str(self.precipChance) + "%",
                           font=self.textfont.font, fill=(0, 0, 0))

        return self.image