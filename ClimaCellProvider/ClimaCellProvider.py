# PyWeather 3 - ClimaCell Provider Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

import requests
import time


class ClimaCellProvider:
    def __init__(self, apikey, latitude, longitude, fields, interval):
        self.apikey = apikey
        self.latitude = latitude
        self.longitude = longitude
        self.fields = fields
        self.interval = interval
        self.data = None

    def request(self):
        climadata = requests.get(
            "https://data.climacell.co/v4/timelines?location={}%2C{}&fields={}&timesteps={}".format(
                self.latitude, self.longitude, self.fields, self.interval), headers={"apikey": self.apikey})
        self.data = climadata.json()

    def getdata(self, interval, field):
        if field == "startTime":
            return self.data['data']['timelines'][0]['intervals'][interval]['startTime']

        return self.data['data']['timelines'][0]['intervals'][interval]['values'][field]

    def translateicon(self, code, icondt, sunrisetime, sunsettime):
        timestamp = icondt.timestamp()
        code = str(code)
        codetranslate = {"4201": u"\uf019", "4001": u"\uf019", "4200": u"\uf01a", "6201": u"\uf017",
        "6001": u"\uf017", "6200": u"\uf017", "6000": u"\uf017", "4000": u"\uf01c", "7101": u"\uf0b5",
        "7000": u"\uf0b5", "7102": u"\uf017", "5000": u"\uf01b", "5100": u"\uf01b", "5001": u"\uf0b5",
        "8000": u"\uf01e", "2100": u"\uf014", "2000": u"\uf014", "1001": u"\uf013"}

        codetranslate_sunup = {"1101": u"\uf002", "1102": u"\uf002", "1100": u"\uf002", "1000": u"\uf00d"}
        codetranslate_sundown = {"1101": u"\uf086", "1102": u"\uf086", "1100": u"\uf086", "1000": u"\uf02e"}

        if code != '1101' and code != '1100' and code != '1000' and code != '1102':
            return codetranslate[code]

        if timestamp <= sunrisetime:
            return codetranslate_sundown[code]
        elif timestamp <= sunsettime:
            return codetranslate_sunup[code]
        else:
            return codetranslate_sundown[code]

