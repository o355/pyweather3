# PyWeather 3
# (c) 2021 Owen McGinley
# Licensed under MIT License

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from DarkSkyProvider.DarkSkyProvider import DarkSkyProvider
from ClimaCellProvider.ClimaCellProvider import ClimaCellProvider
from Fonts.font import Font
from AWSUpload.AWSUpload import AWSUpload
from modules.currentsummary import CurrentSummary
from modules.detailsummary import DetailSummary
from modules.indoorsummary import IndoorSummary
from modules.minutelysummary import MinutelySummary
from modules.dailysummary import DailySummary
from modules.hourlysummary import HourlySummary
from modules.summarymodule import SummaryModule
from SHTC3Provider.C3Provider import C3Provider
from PMSA003Provider.PMSA003Provider import PMSA003Provider
from DPS310Provider.DPS310Provider import DPS310Provider
from BH1750Provider.BH1750Provider import BH1750Provider
from PIL import Image, ImageDraw
import PIL.ImageOps
import board
import busio
from configparser import ConfigParser

config = ConfigParser()
config.read("config.ini")

aws_enabled = config.getboolean("AWS", "enabled")
aws_access_key = config.get("AWS", "access_key")
aws_secret_key = config.get("AWS", "secret_key")
aws_dbname = config.get("AWS", "dbname")
aws_region = config.get("AWS", "region")
aws_purgetime = config.getint("AWS", "purgetime")

darksky_key = config.get("DARKSKY", "key")

climacell_key = config.get("CLIMACELL", "key")

location_latitude = config.getfloat("LOCATION", "latitude")
location_longitude = config.getfloat("LOCATION", "longitude")

inky_disable = config.getboolean("INKY", "disable")
inky_flipped = config.getboolean("INKY", "flipped")

if not inky_disable:
    from inky.inky_uc8159 import Inky
import sys

iconfont = Font("fontfiles/weathericons-regular-webfont.ttf", 32, 24, 20)
textfont = Font("fontfiles/Roboto-Regular.ttf", 28, 20, 12)


dsp = DarkSkyProvider(apikey=darksky_key, latitude=location_latitude, longitude=location_longitude)
dsp.requestdata()

csp = ClimaCellProvider(apikey=climacell_key, latitude=location_latitude, longitude=location_longitude, interval="15m", fields="weatherCode,temperature,windSpeed,windDirection,precipitationProbability")
csp.request()

i2c = busio.I2C(board.SCL, board.SDA)
slow_i2c = busio.I2C(board.SCL, board.SDA, frequency=100000)

shtc3 = C3Provider(i2c)
bh1750 = BH1750Provider(i2c)
dps310 = DPS310Provider(i2c)
pmsa003 = PMSA003Provider(slow_i2c, None)

cs = CurrentSummary(dsp, 375, 112, textfont, iconfont)
csout = cs.render()

#ds = DetailSummary(dsp, 150, 112, textfont, iconfont)
#dsout = ds.render()

ins = IndoorSummary(225, 112, textfont, iconfont, shtc3, dps310, pmsa003, bh1750, dsp)
insout = ins.render()
try:
    hourlysummary = SummaryModule(200, 112, dsp.getminutely(0, "summary"), textfont)
except KeyError:
    hourlysummary = SummaryModule(200, 112, "Hourly summary is not available.", textfont)

hourlysummaryout = hourlysummary.render()

minutelysummary = MinutelySummary(400, 112, textfont, iconfont, csp, dsp)
minutelysummaryout = minutelysummary.render()

hourlyblocks = HourlySummary(600, 112, textfont, iconfont, dsp)
hourlyblocksout = hourlyblocks.render()

dailyblocks = DailySummary(600, 112, textfont, iconfont, dsp)
dailyblocksout = dailyblocks.render()

awsupload = AWSUpload(aws_access_key, aws_secret_key, aws_region, aws_dbname, aws_purgetime)
if aws_enabled:
    indoortemp = shtc3.readtemp("F")
    outdoortempd = indoortemp - dsp.getcurrently("temperature")
    indoorhum = shtc3.readhum()
    indoorhumd = indoorhum - (dsp.getcurrently("humidity") * 100)
    indoorpress = dps310.readpressure()
    indoordata = pmsa003.read()
    indoorpm10 = indoordata['pm10 env']
    indoorpm25 = indoordata['pm25 env']
    indoorpm100 = indoordata['pm100 env']
    indooraqi = pmsa003.usaqi(indoorpm25)
    indoorlux = bh1750.read()
    dewpoint = shtc3.get_dew_point(indoortemp, indoorhum)
    awsupload.upload(indoortemp, outdoortempd, indoorhum, indoorhumd, indoorpress, indoorpm10, indoorpm25, indoorpm100, indooraqi, dewpoint)
    awsupload.delete()
## ALL THE OTHER MODULES GO HERE!

if not inky_disable:
    display = Inky()
    saturation = 1.0

canvas = Image.new("RGB", (600, 448), (255, 255, 255))
canvasdraw = ImageDraw.Draw(canvas)
canvas.paste(csout, (0, 0))
canvas.paste(insout, (375, 0))
canvas.paste(hourlysummaryout, (0, 112))
canvas.paste(minutelysummaryout, (200, 112))
canvas.paste(hourlyblocksout, (0, 224))
canvas.paste(dailyblocksout, (0, 336))
canvasdraw.line((0, 112, 600, 112), fill=0, width=2)
canvasdraw.line((375, 0, 375, 112), fill=0, width=2)
canvasdraw.line((0, 224, 600, 224), fill=0, width=2)
canvasdraw.line((200, 112, 200, 224), fill=0, width=2)
canvasdraw.line((300, 112, 300, 224), fill=0, width=2)
canvasdraw.line((400, 112, 400, 224), fill=0, width=2)
canvasdraw.line((500, 112, 500, 224), fill=0, width=2)
canvasdraw.line((0, 336, 600, 336), fill=0, width=2)
canvasdraw.line((100, 224, 100, 448), fill=0, width=2)
canvasdraw.line((200, 224, 200, 448), fill=0, width=2)
canvasdraw.line((300, 224, 300, 448), fill=0, width=2)
canvasdraw.line((400, 224, 400, 448), fill=0, width=2)
canvasdraw.line((500, 224, 500, 448), fill=0, width=2)
if not inky_disable:
    if inky_flipped:
        canvas = PIL.ImageOps.flip(canvas)
        canvas = PIL.ImageOps.mirror(canvas)
    display.set_image(canvas, saturation=saturation)
    display.show(busy_wait=False)
else:
    canvas.show()


