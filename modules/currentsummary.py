# PyWeather 3 - Current Summary Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

from PIL import Image, ImageDraw
from datetime import datetime


class CurrentSummary:
    """
    :param dsp Dark Sky Provider
    """
    def __init__(self, dsp, width, height, textfont, iconfont):
        self.dsp = dsp
        self.width = width
        self.height = height
        self.iconfont = iconfont
        self.textfont = textfont
        self.image = Image.new("RGB", (self.width, self.height), (255, 255, 255))
        self.draw = ImageDraw.Draw(self.image)

    def render(self):
        W, H = (50, 50)
        icon = self.dsp.translateicon(self.dsp.getcurrently("icon"), summary=self.dsp.getcurrently("summary"))
        w, h = self.draw.textsize(icon, font=self.iconfont.large_font)
        tempw, temph = self.draw.textsize(str(int(round(self.dsp.getcurrently("temperature"), 0))) + "°F", font=self.textfont.large_font)
        self.draw.text((10 + ((W-w) / 2), 0), self.dsp.translateicon(self.dsp.getcurrently("icon"), summary=self.dsp.getcurrently("summary")), font=self.iconfont.large_font, fill=(0, 0, 0))
        self.draw.text((65, 0), str(int(round(self.dsp.getcurrently("temperature"), 0))) + "°F", font=self.textfont.large_font, fill=(0, 0, 0))
        self.draw.text((65, 27), self.dsp.getcurrently("summary").replace("Humid and ", "").split(" and")[0] +
                       ("!" if "snow" in self.dsp.getcurrently("summary").lower() else ""), font=self.textfont.font, fill=(0, 0, 0))

        self.draw.text((15, self.height - 58), u"\uf051", font=self.iconfont.font, fill=(0, 0, 0))
        denom = datetime.fromtimestamp(self.dsp.getdaily(0, "sunriseTime"), tz=self.dsp.tz).strftime("%p")
        if denom == "AM":
            denom = "a"
        elif denom == "PM":
            denom = "p"

        srtime = datetime.fromtimestamp(self.dsp.getdaily(0, "sunriseTime"), tz=self.dsp.tz).strftime("%-I:%M")
        srtime = srtime + denom
        # 21px = 19px font + 2px buff
        self.draw.text((52, self.height - 53), srtime, font=self.textfont.font, fill=(0, 0, 0))

        try:
            self.draw.text((115, self.height - 58), u"\uf052", font=self.iconfont.font, fill=(0, 0, 0))
            denom = datetime.fromtimestamp(self.dsp.getdaily(0, "sunsetTime"), tz=self.dsp.tz).strftime("%p")
            if denom == "AM":
                denom = "a"
            elif denom == "PM":
                denom = "p"

            srtime = datetime.fromtimestamp(self.dsp.getdaily(0, "sunsetTime"), tz=self.dsp.tz).strftime("%-I:%M")
            srtime = srtime + denom
            self.draw.text((152, self.height - 53), srtime, font=self.textfont.font, fill=(0, 0, 0))
        except KeyError:
            pass

        now = datetime.now()
        denom = now.strftime("%p")
        if denom == "AM":
            denom = "a"
        elif denom == "PM":
            denom = "p"

        updatew, updatey = self.draw.textsize(u"\uf03e", font=self.iconfont.font)
        self.draw.text((17, self.height - 34), u"\uf03e", font=self.iconfont.font, fill=(0, 0, 0))
        self.draw.text((24 + updatew, self.height - 27), now.strftime("%-I:%M %p"), font=self.textfont.font, fill=(0, 0, 0))

        self.draw.text((65 + tempw + 20, 2), u"\uf053", font=self.iconfont.small_font, fill=(0, 0, 0))
        self.draw.text((65 + tempw + 40, 2), str(int(round(self.dsp.getcurrently("apparentTemperature"), 0))) + "°F", font=self.textfont.font, fill=(0, 0, 0))

        windtext = str(int(round(self.dsp.getcurrently("windSpeed"), 0))) + " G " + str(int(round(self.dsp.getcurrently("windGust"), 0)))
        windw, windy = self.draw.textsize(windtext, font=self.textfont.font)
        self.draw.text((self.width - windw - 39, -13), self.dsp.translatebearing(self.dsp.getcurrently("windBearing")), font=self.iconfont.large_font, fill=(0, 0, 0))
        self.draw.text((self.width - windw - 15, 0), windtext, font=self.textfont.font, fill=(0, 0, 0))

        humtext = str(int(self.dsp.getcurrently("humidity") * 100)) + "%"
        humx, humy = self.draw.textsize(humtext, font=self.textfont.font)
        self.draw.text((self.width - windw - 41, 22), u"\uf07a", font=self.iconfont.font, fill=(0, 0, 0))
        self.draw.text((self.width - humx - 15, 27), humtext, font=self.textfont.font, fill=(0, 0, 0))

        cloudtext = str(int(self.dsp.getcurrently("cloudCover") * 100)) + "%"
        cloudx, cloudy = self.draw.textsize(cloudtext, font=self.textfont.font)
        self.draw.text((self.width - windw - 44, 49), u"\uf041", font=self.iconfont.font, fill=(0, 0, 0))
        self.draw.text((self.width - cloudx - 15, 54), cloudtext, font=self.textfont.font, fill=(0, 0, 0))

        preciptext = str(int(self.dsp.getcurrently("precipProbability") * 100)) + "%"
        precipx, precipy = self.draw.textsize(preciptext, font=self.textfont.font)
        self.draw.text((self.width - windw - 45, 76), u"\uf084", font=self.iconfont.font, fill=(0, 0, 0))
        self.draw.text((self.width - precipx - 15, 81), preciptext, font=self.textfont.font, fill=(0, 0, 0))

        return self.image
