# PyWeather 3 - Daily Summary Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

from PIL import Image, ImageDraw
from Pod.Pod import Pod
from datetime import datetime


class DailySummary:
    def __init__(self, width, height, textfont, iconfont, dsp):
        self.width = width
        self.height = height
        self.textfont = textfont
        self.iconfont = iconfont
        self.dsp = dsp
        self.image = Image.new("RGB", (self.width, self.height), (255, 255, 255))
        self.draw = ImageDraw.Draw(self.image)

    def render(self):
        for i in range(0, 6):
            dtobj = datetime.fromtimestamp(self.dsp.getdaily(i, "time"), tz=self.dsp.gettzobj())
            temperature = str(int(round(self.dsp.getdaily(i, "temperatureHigh"), 0)))
            temperaturelow = str(int(round(self.dsp.getdaily(i, "temperatureLow"), 0)))
            precipChance = int(self.dsp.getdaily(i, "precipProbability") * 100)
            windSpeed = self.dsp.getdaily(i, "windSpeed")
            windDirection = self.dsp.getdaily(i, "windBearing")
            icon = self.dsp.translateicon(self.dsp.getdaily(i, "icon"), rate=self.dsp.getdaily(i, "precipIntensityMax"))
            pod = Pod(int(self.width / 6), self.height, self.textfont, self.iconfont, dtobj, "%a", False, icon, temperature, precipChance, windSpeed, windDirection, 25, self.dsp, temperaturelow)
            self.image.paste(pod.render(), (i * 100, 0))

        return self.image