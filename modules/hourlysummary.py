# PyWeather 3 - Hourly Summary Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

from PIL import Image, ImageDraw
from Pod.Pod import Pod
from datetime import datetime


class HourlySummary:
    def __init__(self, width, height, textfont, iconfont, dsp):
        self.width = width
        self.height = height
        self.textfont = textfont
        self.iconfont = iconfont
        self.dsp = dsp
        self.image = Image.new("RGB", (self.width, self.height), (255, 255, 255))
        self.draw = ImageDraw.Draw(self.image)

    def render(self):
        for i in range(2, 8):
            dtobj = datetime.fromtimestamp(self.dsp.gethourly(i, "time"), tz=self.dsp.gettzobj())
            temperature = str(int(round(self.dsp.gethourly(i, "temperature"), 0)))
            precipChance = int(self.dsp.gethourly(i, "precipProbability") * 100)
            windSpeed = self.dsp.gethourly(i, "windSpeed")
            windDirection = self.dsp.gethourly(i, "windBearing")
            icon = self.dsp.translateicon(self.dsp.gethourly(i, "icon"), rate=self.dsp.gethourly(i, "precipIntensity"))
            pod = Pod(int(self.width / 6), self.height, self.textfont, self.iconfont, dtobj, "%-I %p", False, icon, temperature, precipChance, windSpeed, windDirection, 25, self.dsp, None)
            self.image.paste(pod.render(), ((i - 2) * 100, 0))

        return self.image