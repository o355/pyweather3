# PyWeather 3 - Detailed Summary Module (Not In Use)
# (c) 2021 Owen McGinley
# Licensed under MIT License

from PIL import Image, ImageDraw


class DetailSummary:
    def __init__(self, dsp, width, height, textfont, iconfont):
        self.dsp = dsp
        self.width = width
        self.height = height
        self.textfont = textfont
        self.iconfont = iconfont

        self.image = Image.new("RGB", (self.width, self.height), (255, 255, 255))
        self.draw = ImageDraw.Draw(self.image)

    def render(self):
        self.draw.text((10, 10), "Wind: " + str(int(round(self.dsp.getcurrently("windSpeed"), 0))) + " mph", font=self.textfont.font, fill=(0, 0, 0))
        self.draw.text((10, 29), "Feels Like: " + str(int(round(self.dsp.getcurrently("apparentTemperature"), 0))) + "°F", font=self.textfont.font, fill=(0, 0, 0))
        self.draw.text((10, 48), "Humidity: " + str(int(self.dsp.getcurrently("humidity") * 100)) + "%", font=self.textfont.font, fill=(0, 0, 0))
        self.draw.text((10, 67), "Visibility: " + str(int(self.dsp.getcurrently("visibility"))) + " mi", font=self.textfont.font, fill=(0, 0, 0))

        return self.image