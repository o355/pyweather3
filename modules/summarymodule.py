# PyWeather 3 - Summary Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

from PIL import Image, ImageDraw
import textwrap


class SummaryModule:
    def __init__(self, width, height, text, textfont):
        self.width = width
        self.height = height
        self.text = text
        self.textfont = textfont
        self.image = Image.new("RGB", (self.width, self.height), (255, 255, 255))
        self.draw = ImageDraw.Draw(self.image)

    def render(self):
        wrap = textwrap.TextWrapper(width=self.width / 10)
        wraps = wrap.wrap(text=self.text)
        newline_wrap = ""
        for word in wraps:
            newline_wrap = newline_wrap + word + "\n"

        self.draw.text((10, 10), newline_wrap, font=self.textfont.font, fill=(0, 0, 0))

        return self.image