# PyWeather 3 - Minutely Summary Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

from PIL import Image, ImageDraw
from Pod.Pod import Pod
import dateutil.parser
import time


class MinutelySummary:
    def __init__(self, width, height, textfont, iconfont, csp, dsp):
        self.width = width
        self.height = height
        self.textfont = textfont
        self.iconfont = iconfont
        self.csp = csp
        self.dsp = dsp
        self.image = Image.new("RGB", (self.width, self.height), (255, 255, 255))
        self.draw = ImageDraw.Draw(self.image)

    def render(self):
        # Get delta from DS temp to climacell
        try:
            tempdelta = ((self.csp.getdata(0, "temperature") * (9 / 5)) + 32) - self.dsp.getcurrently("temperature")
        except KeyError:
            time.sleep(15)
            self.csp.getdata()
            tempdelta = ((self.csp.getdata(0, "temperature") * (9 / 5)) + 32) - self.dsp.getcurrently("temperature")

        for i in range(1, 5):
            timestamp = self.csp.getdata(i, "startTime")
            datetime = dateutil.parser.parse(timestamp)
            to_zone = self.dsp.gettzobj()
            localdt = datetime.astimezone(to_zone)
            try:
                if self.dsp.getminutely((i * 15) - 1, "precipIntensity") > 0:
                    if self.dsp.getminutely((i * 15) - 1, "precipProbability") > 0.20:
                        icon = self.dsp.translateicon(self.dsp.getminutely((i * 15) - 1, "precipType"), rate=self.dsp.getminutely((i * 15) - 1, "precipIntensity"))
                    else:
                        icon = self.csp.translateicon(self.csp.getdata(i, "weatherCode"), localdt, self.dsp.getsunrisetime(),
                                                      self.dsp.getsunsettime())
                else:
                    icon = self.csp.translateicon(self.csp.getdata(i, "weatherCode"), localdt, self.dsp.getsunrisetime(), self.dsp.getsunsettime())
            except KeyError:
                icon = self.csp.translateicon(self.csp.getdata(i, "weatherCode"), localdt, self.dsp.getsunrisetime(),
                                              self.dsp.getsunsettime())
            temperature = str(int(round(((self.csp.getdata(i, "temperature") * (9 / 5)) + 32) - tempdelta, 0)))
            try:
                precipchance = int(self.dsp.getminutely((i * 15) - 1, "precipProbability") * 100)
            except KeyError:
                precipchance = int(self.csp.getdata(i, "precipitationProbability"))
            windSpeed = self.csp.getdata(i, "windSpeed")
            windDirection = self.csp.getdata(i, "windDirection")
            pod = Pod(int(self.width / 4), self.height, self.textfont, self.iconfont, localdt, "%-I:%M", True, icon, temperature, precipchance, windSpeed, windDirection, 0, self.dsp, None)
            self.image.paste(pod.render(), ((i - 1) * 100, 0))

        return self.image