# PyWeather 3 - Indoor Summary Module
# (c) 2021 Owen McGinley
# Licensed under MIT License

from PIL import Image, ImageDraw
import time
import math
import textwrap

class IndoorSummary:
    def __init__(self, width, height, textfont, iconfont, temphum, pressure, aqi, lux, dsp):
        self.width = width
        self.height = height
        self.textfont = textfont
        self.iconfont = iconfont
        self.temphum = temphum
        self.pressure = pressure
        self.aqi = aqi
        self.lux = lux
        self.dsp = dsp
        self.image = Image.new("RGB", (self.width, self.height), (255, 255, 255))
        self.draw = ImageDraw.Draw(self.image)

    def render(self):
        tempreading = self.temphum.readtemp("F")
        temptext = str(round(tempreading, 1)) + "°F"
        tempx, tempy = self.draw.textsize(temptext, font=self.textfont.large_font)
        insidetext = "inside"
        insidex, insidey = self.draw.textsize(insidetext, font=self.textfont.font)
        totalwidth = tempx + 9 + insidex
        self.draw.text(((self.width - totalwidth) / 2, 0), temptext, font=self.textfont.large_font, fill=(0, 0, 0))
        self.draw.text((((self.width - totalwidth) / 2) + tempx + 9, 4), "inside", font=self.textfont.font, fill=(0, 0, 0))
        self.draw.line((0, tempy + 8, self.width, tempy + 8), fill=0, width=2)

        alertsavail = False
        alertsindex = 0
        alertsindex_secondary = 1
        alerts_secondary_start = 99999999999
        try:
            alert = self.dsp.getalerts(0, "title")
            alertsavail = True
        except KeyError:
            pass

        if alertsavail:
            starttime = self.dsp.getalerts(0, "time")
            expiretime = self.dsp.getalerts(0, "expires")

            alertslen = self.dsp.getalertslength()

            if alertslen > 1:
                for i in reversed(range(1, alertslen)):
                    if self.dsp.getalerts(i, "time") < alerts_secondary_start:
                        alerts_secondary_start = self.dsp.getalerts(i, "time")
                        alertsindex_secondary = i

            if time.time() + 10800 < starttime < expiretime and time.time() < starttime:
                if alertslen > 1:
                    for i in reversed(range(1, alertslen)):
                        if self.dsp.getalerts(i, "time") < alerts_secondary_start:
                            alerts_secondary_start = self.dsp.getalerts(i, "time")
                            alertsindex = i
                else:
                    alertsavail = False

            if alertsindex == 0 and alertslen > 1 and time.time() < starttime:
                alertsindex = alertsindex_secondary

        if alertsavail:
            starttime = self.dsp.getalerts(alertsindex, "time")
            expiretime = self.dsp.getalerts(alertsindex, "expires")
            alerttext = ""
            alerttext = alerttext + self.dsp.getalerts(alertsindex, "title")
            timediff_start = starttime - time.time()
            timediff_end = expiretime - time.time()

            if timediff_start > 0 and starttime < expiretime:
                if timediff_start < 3600:
                    alerttext = alerttext + " begins in " + str(int(round(math.ceil(timediff_start / 60), 0))) + " minute"
                    if int(round(math.ceil(timediff_start / 60), 0)) != 1:
                        alerttext = alerttext + "s"
                    alerttext = alerttext + "."
                else:
                    alerttext = alerttext + " begins in " + str(int(round(math.ceil(timediff_start / 3600), 0))) + " hour"
                    if int(round(math.ceil(timediff_start / 3600), 0)) != 1:
                        alerttext = alerttext + "s"
                    alerttext = alerttext + "."
            else:
                if timediff_end < 3600:
                    alerttext = alerttext + " ends in " + str(
                        int(round(math.ceil(timediff_end / 60), 0))) + " minute"
                    if int(round(math.ceil(timediff_end / 60), 0)) != 1:
                        alerttext = alerttext + "s"
                    alerttext = alerttext + "."
                else:
                    alerttext = alerttext + " ends in " + str(
                        int(round(math.ceil(timediff_end / 3600), 0))) + " hour"
                    if int(round(math.ceil(timediff_end / 3600), 0)) != 1:
                        alerttext = alerttext + "s"
                    alerttext = alerttext + "."

            wrap = textwrap.TextWrapper(width=int(self.width / 10))
            wraps = wrap.wrap(text=alerttext)
            newline_wrap = ""
            for word in wraps:
                newline_wrap = newline_wrap + word + "\n"

            self.draw.text((10, tempy + 12), newline_wrap, font=self.textfont.font, fill=(0, 0, 0))
        else:
            self.draw.text((10, tempy + 15), u"\uf07a", font=self.iconfont.font, fill=(0, 0, 0))
            self.draw.text((40, tempy + 20), str(round(int(self.temphum.readhum()))) + "%", font=self.textfont.font, fill=(0, 0, 0))

            aqidata = self.aqi.read()

            aqinum = self.aqi.usaqi(aqidata['pm25 env'])
            aqitext = str(int(round(aqinum, 0)))
            aqiw, aqiy = self.draw.textsize(aqitext, font=self.textfont.font)
            if aqinum < 100:
                self.draw.text(((self.width / 2) + 5, tempy + 15), u"\uf062", font=self.iconfont.font, fill=(0, 0, 0))
            else:
                self.draw.text(((self.width / 2) + 5, tempy + 15), u"\uf074", font=self.iconfont.font, fill=(0, 0, 0))
            self.draw.text(((self.width / 2) + 35, tempy + 20), aqitext + " AQI", font=self.textfont.font, fill=(0, 0, 0))

            humx, humy = self.draw.textsize(u"\uf07a", font=self.iconfont.font)
            self.draw.text((10, tempy + 15 + humy), u"\uf079", font=self.iconfont.font, fill=(0, 0, 0))
            self.draw.text((40, tempy + 20 + humy + 2), str(int(round(self.pressure.readpressure(), 0))) + "mb", font=self.textfont.font, fill=(0, 0, 0))

            # TODO: If over 10000 lux, change abbreviation to lx
            luxread = self.lux.read()
            self.draw.text(((self.width / 2) + 5, tempy + 15 + humy + 5), u"\uf00d", font=self.iconfont.small_font, fill=(0, 0, 0))
            if luxread >= 10000:
                self.draw.text(((self.width / 2) + 35, tempy + 20 + humy + 2),
                               str(int(round(luxread, 0))) + " lx", font=self.textfont.font, fill=(0, 0, 0))
            else:
                self.draw.text(((self.width / 2) + 35, tempy + 20 + humy + 2), str(int(round(luxread, 0))) + " lux", font=self.textfont.font, fill=(0, 0, 0))

        return self.image
